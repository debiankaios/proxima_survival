--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Ores
]]--

minetest.register_ore({
  ore_type        = "blob",
  ore             = "default:clay",
  wherein         = {"pxs_default:proxima_sand"},
  clust_scarcity  = 32 * 32 * 32,
  clust_size      = 3,
  y_max           = 15,
  y_min           = -15,
  noise_threshold = 0.0,
  noise_params    = {
    offset = 0.5,
    scale = 0.2,
    spread = {x = 5, y = 5, z = 5},
    seed = -316,
    octaves = 1,
    persist = 0.0
  },
})

minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pxs_default:proxima_stone_with_coal",
  wherein        = "pxs_default:proxima_stone",
  clust_scarcity = 8 * 8 * 8,
  clust_num_ores = 8,
  clust_size     = 3,
  y_max          = 64,
  y_min          = -127,
})
