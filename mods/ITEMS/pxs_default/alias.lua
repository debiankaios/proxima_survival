--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Alias
]]--

minetest.register_alias("pxs_default:mars_stone", "default:mars_stone")
minetest.register_alias("pxs_default:proxima_stone", "default:proxima_stone")
minetest.register_alias("pxs_default:proxima_sand", "default:proxima_sand")
minetest.register_alias("pxs_default:tiny_proxima_stone", "default:little_proxima_stone")
minetest.register_alias("pxs_default:tiny_proxima_stone", "pxs_default:little_proxima_stone")
minetest.register_alias("pxs_default:alienflower", "default:alienflower")
minetest.register_alias("pxs_default:proxima_stone_stick", "default:proxima_stone_stick")
minetest.register_alias("pxs_default:topas", "default:topas")
minetest.register_alias("pxs_default:blue_topas", "default:blue_topas")
minetest.register_alias("pxs_default:red_topas", "default:red_topas")
minetest.register_alias("pxs_default:yellow_topas", "default:yellow_topas")
minetest.register_alias("pxs_default:orange_topas", "default:orange_topas")
minetest.register_alias("pxs_default:gray_topas", "default:gray_topas")
minetest.register_alias("pxs_default:purple_topas", "default:purple_topas")
minetest.register_alias("pxs_default:pink_topas", "default:pink_topas")
minetest.register_alias("pxs_default:brown_topas", "default:brown_topas")
minetest.register_alias("pxs_default:green_topas", "default:green_topas")
minetest.register_alias("pxs_default:op_posometer", "default:op_posometer")
minetest.register_alias("pxs_default:deoxidizer_1", "default:deoxidizer_1")
minetest.register_alias("pxs_default:alienflower_original", "default:alienflower_original")
-- minetest.register_alias("", "")
