## 1. Hardness

#### How it work:
Hardnessstages it give 1 - 15. Every tool can pick a stage higher. The scale is similar to that of mohs. For Alternative you can use default_system

#### Scala:
Stage 1: Can pick by all
Stage 2: Can pick by hand
Stage 3: Can pick by wood
Stage 4: Can pick by stone or copper
Stage 5: Can pick by bronze
Stage 6: Can pick by iron
Stage 7: Can pick by steel
Stage 8: Can pick by quarz
Stage 9: Can pick by corundum
Stage 10: Can pick by topas
Stage 11: Can pick by diamond
Stage 12: Can pick by Q-carbon
Stage 13: Can pick by Mese
Stage 14: Can pick by nothing make a material with a mod
Stage 15: Can pick by nothing make a material with a mod



#### Tools:
hand = handy
pickaxe = picky
axe = axey
shovel = spady
sword = sharpness
hoe = hoey
any = any